#!/usr/bin/env python3
import psycopg2
import sys
import pandas as pd
import argparse
import dateparser
import progressbar
import quilt


def parse_args():
    parser = argparse.ArgumentParser(
        description="Export metrics to Panda Dataframes 🐼")
    parser.add_argument('-u', '--username', help='Postgresql Username',
                        default='postgres')
    parser.add_argument('-o', '--host', help='Postgresql Host', required=True)
    parser.add_argument('-p', '--password', help='Postgresql Password',
                        required=True)
    parser.add_argument('-d', '--database', help='Postgresql Database',
                        required=True)
    parser.add_argument('-s', '--start', help='Start time for query',
                        default='6 minutes ago')
    parser.add_argument('-e', '--end', help='End time for query',
                        default='1 minute ago')
    # parser.add_argument('identifier', help='Identifier, usually USERNAME/PKG')
    return parser.parse_args()


def main():
    args = parse_args()

    conn = psycopg2.connect(
        dbname=args.database,
        user=args.username,
        password=args.password,
        sslmode="require",
        host=args.host
    )
    cur = conn.cursor()

    start = dateparser.parse(args.start)
    end = dateparser.parse(args.end)

    quilt.build("duke/foo")
    from quilt.data.duke import foo

    # Select metric names
    print(
        "Checking to see which metric names are available between %s and %s"
        % (start, end))
    cur.execute("""
SELECT DISTINCT name
FROM metrics
WHERE time BETWEEN '%s' and '%s';
""" % (start, end))
    metric_names = [r[0] for r in (cur.fetchall())]

    metric_name_count = len(metric_names)
    print("Found %s metric names" % metric_name_count)

    # Set up a progresbar since this is noisy
    widgets = [progressbar.Percentage(), progressbar.Bar(), progressbar.ETA()]
    bar = progressbar.ProgressBar(
        widgets=widgets, max_value=metric_name_count).start()
    i = 0

    for metric_name in metric_names:
        # print("Converting %s to a dataframe" % metric_name)

        # Get a list of all possible metric labels
        columns = [metric_name]
        cur.execute("""
SELECT (ARRAY(SELECT jsonb_object_keys(labels)))
FROM metrics_labels
WHERE metric_name = '%s'
""" % (metric_name))
        # Feels like there is a better way to do this...
        for row in cur.fetchall():
            for item in row[0]:
                if item not in columns:
                    columns.append(item)

        cur.execute("""
SELECT value, labels
FROM metrics
WHERE name = '%s'
  AND time BETWEEN '%s' and '%s';""" % (metric_name, start, end))
        metric_data = {}
        index = 0
        for value, row in cur.fetchall():
            metric_data[index] = {
                'metric_name': metric_name,
                'metric_value': value,
            }
            for label_key, label_value in row.items():
                metric_data[index][label_key] = label_value
            index = index + 1
        df = pd.DataFrame(metric_data)
        df = df.transpose()
        foo._set(['bar'], df)
        foo.bar()

        i += 1
        bar.update(i)
    bar.finish()
    quilt.export("duke/foo", "./output")
    return 0


if __name__ == "__main__":
    sys.exit(main())
